from fastapi import FastAPI
from app.core.config import settings
from app.api import router
from app.api.v1 import auth


def create_app() -> FastAPI:
    app = FastAPI(
        title = settings.PROJECT_NAME,
        docs_url = "/api/docs",
        openapi_url = "/api"
    )
    app.include_router(router, prefix = settings.API_V1_STR)

    return app
