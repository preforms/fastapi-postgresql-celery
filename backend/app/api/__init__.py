from fastapi import APIRouter, Depends
from app.core.auth import get_current_active_user
from app.api import v1
from app.api.v1 import auth


router = APIRouter()


router.include_router(auth.router, tags=["auth"])

router.include_router(
    v1.router,
    prefix="/v1",
    dependencies=[Depends(get_current_active_user)],
)
