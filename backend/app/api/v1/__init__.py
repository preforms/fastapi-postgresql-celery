from fastapi import APIRouter,Depends
from app.core.auth import get_current_active_user
from app.api.v1 import users


router = APIRouter()

router.include_router(users.router, prefix="/users", tags=["users"], dependencies=[Depends(get_current_active_user)])

